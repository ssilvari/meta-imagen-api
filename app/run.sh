#!/bin/bash

# Flag (in case you want to run in daemon mode)
FLAG=$1

# Set parameters up
CONTAINER_NAME="metaimagen_api"
USER="sssilvar"

IMG_NAME=$USER"/"${CONTAINER_NAME}
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "\n\n[  OK  ] Stoping container"
STOP_CONT="docker stop metaimagen_api metaimagen_db"
eval ${STOP_CONT}

echo -e "\n\n[  OK  ] Deleting container"
DEL_CONT="docker rm metaimagen_api metaimagen_db"
eval ${DEL_CONT}

echo -e "\n\n[  OK  ] Deleting image"
DEL_IMG="docker rmi app_app"
eval ${DEL_IMG}

echo -e "\n\n[  OK  ] Creating the new image: "${IMG_NAME}
#CRE_IMG="docker build -t "${IMG_NAME}" --build-arg proxy="$PROXY" "$CURRENT_DIR
CRE_IMG="docker-compose up --build "${FLAG}
eval ${CRE_IMG}