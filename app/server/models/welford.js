const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('../config/database')


const WelfordSchema = mongoose.Schema({
    centers: {
        type: [String],
        required: true
    },
    centers_done: {
        type: [String],
        required: false
    },
    md5: {
        type: Number,
        required: false
    },
    busy: {
        type: Boolean,
        default: false,
        required: false
    },
    done: {
        type:Boolean,
        required: false,
        default: false
    },
    updated: {
        type: Date, default: Date.now
    }
});

const Welford = module.exports = mongoose.model('Welford', WelfordSchema);

module.exports.addWelford = function (newWelford, callback) {
    newWelford.save(callback);
};

module.exports.getStatsById = function (id, callback) {
    Welford.findById(id, callback);
};
