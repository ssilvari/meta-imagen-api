const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const UploadSchema = mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    postedBy: {
        type: String,
        required: true
    },
    md5: {
        type: String,
        required: false
    },
    description: {
        type: String,
        required: false
    },
    date:{
        type: Date, default: Date.now
    }
});

const Upload = module.exports = mongoose.model('Upload', UploadSchema);

module.exports.getUploadById = function (id, callback) {
    Upload.findById(id, callback);
};

module.exports.addUpload = function (newUpload, callback) {
    newUpload.save(callback);
};
