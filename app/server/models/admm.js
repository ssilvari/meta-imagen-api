const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const AdmmSchema = mongoose.Schema({
    w_tilde: {
        type: String,
        required: false
    },
    rho: {
        type: Number,
        required: true
    },
    iteration: {
        type: Number,
        required: true
    },
    number_of_iterations: {
        type: Number,
        required: true
    },
    md5: {
        type: [String],
        required: false
    },
    iteration_finished: {
        type: Boolean,
        default: false,
        required: false
    },
    busy: {
        type: Boolean,
        default: false,
        required: false
    },
    centers: {
        type: [String],
        required: true
    },
    centers_done: {
        type: [String],
        required: false
    },
    number_of_centers: {
        type: Number,
        required: true
    },
    recalculate: {
        type: Boolean,
        required: false
    },
    done: {
        type: Boolean,
        default: false
    },
    observations: {
        type: String,
        required: false
    },
    updated:{
        type: Date, default: Date.now
    }
});

const Admm = module.exports = mongoose.model('Admm', AdmmSchema);

module.exports.getAdmmById = function (id, callback) {
    Admm.findById(id, callback);
};

module.exports.addAdmm = function (newAdmm, callback) {
    newAdmm.save(callback);
};
