const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const crypto = require('crypto');

const config = require('../config/database');
var token = crypto.randomBytes(64).toString('hex');

const CenterSchema = mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    statistics: {
        type: [String],
        required: false
    },
    admm: {
        type: [String],
        required: false
    },
    numberOfSubjects: {
        type: Number,
        required: false
    },
    enabled: {
        type: Boolean,
        required: true
    },
    info: {
        type: String,
        required: false
    },
    version: {
        type: Number,
        default: 0,
        required: false
    },
    updated:{
        type: Date, default: Date.now
    }
});

const Center = module.exports = mongoose.model('Center', CenterSchema);

module.exports.getCenterById = function (id, callback) {
    Center.findById(id, callback);
};

module.exports.addCenter = function (newCenter, callback) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newCenter.password, salt, (err, hash) => {
            if (err) throw err;
            newCenter.password = hash;
            newCenter.save(callback);
        })
    });
};

module.exports.compareKey = function (candidateKey, hash, callback) {
    bcrypt.compare(candidateKey, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
};
