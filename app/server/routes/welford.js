const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const path = require('path');

const Welford = require('../models/welford');

const config = require('../config/database')

// Compare two sets (different order)
function eqSet(as, bs) {
    if (as.size !== bs.size) return false;
    for (var a of as) if (!bs.has(a)) return false;
    return true;
}

/*
* ====== WELFORD INFO (NO FILES) ======
*/

// Get Welford task info
router.get('/', function(req, res, next) {
    // Get last WF
    Welford.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, wf) {
        if (err){
            res.json({success: false, msg: 'There was an error getting the Welford task'})
        } else if (wf != null) {
            res.json({success: true, data: wf})
        } else {
            res.json({success: true, msg: 'No center has published on database', data: null})
        }
    });
});


// Create a new Welford calculation
router.post('/new', function(req, res, next) {
    let newWelford = Welford({
        centers: req.body.centers
    });

    newWelford.save(err => {
        if (err) {
            console.log('Error updating the database!' + err.toString())
            res.json({success: false, msg: 'Could not update Welford task in database'})
        } else {
            res.json({success: true, msg: 'Welford task created succesfully with id: ' + newWelford._id})
        }
    });
});

router.get('/info/:id', function(req, res, next) {
    id = req.params.id;

    Welford.getStatsById(id, (err, wf) =>{
        if (err){
            res.json({success: false, msg: 'There was an error getting the statistics'})
        } else if (wf != null) {
            res.json({success: true, data: wf, msg: 'Data found successfully'});
        } else {
            console.log(wf);
            res.json({success: false, data: null, msg: 'Data not found'})
        }
    });

});

router.put('/status/:id/:status', function(req, res, next){
    id = req.params.id;
    status = req.params.status.toLowerCase() == 'true';

    Welford.getStatsById(id, (err, wf) =>{
        if (err){
            console.log(err.toString())
            res.json({success: false, msg: 'There was an error getting the statistics'})
        } else if (wf != null) {
            wf.busy = status;

            wf.save(err => {
                if (err) {
                    console.log('Error updating the database!' + err.toString())
                    res.json({success: false, msg: 'Could not update Welford task in database'})
                } else {
                    res.json({success: true, msg: 'Welford status updated', data: wf})
                }
            });
        } else {
            console.log(wf);
            res.json({success: false, data: null, msg: 'Data not found'})
        }
    });
});

/*
* ====== WELFORD DATA UPLOAD/DOWNLOAD ======
*/

// Get last update
router.get('/data', function(req, res, next) {
    Welford.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, wf) {
        if (err){
            res.json({success: false, msg: 'There was an error getting the statistics'})
        } else if (wf != null) {
            const filename = path.join(config.upload_folder, 'welford_' + wf._id + '.npz');
            console.log('Downloading file: ' + filename);

            // Send filename to client
            res.download(filename, 'welford.npz', (err) => {
                if (err){
                    console.log('Download failed from center: ');
                } else {
                    console.log('Download successful from center ');
                }
            });
        } else {
            res.json({success: true, msg: 'No center has published on database', data: null})
        }
    });

});

// Post stats
router.post('/data', function(req, res, next) {
    center_id = req.body.id;

    Welford.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, wf) {
        if (err){
            res.json({success: false, msg: 'There was an error getting the statistics'})
        } else if (wf != null) {
            // Upload Data
            let dataFile = req.files.dataFile;
            let centersSet = new Set(wf.centers);
            let doneSet = new Set(wf.centers_done);
            doneSet.add(center_id);

            dataFile.mv(config.upload_folder + 'welford_' + wf._id + '.npz' , function(err) {
                if (err){
                    console.log(err.toString())
                    return res.status(500).send(err);
                }
            });

            wf.statistics = dataFile.md5;
            wf.busy = false;
            wf.done = eqSet(doneSet, centersSet);
            wf.centers_done = Array.from(doneSet);

            wf.save(err => {
                if (err) {
                    res.json({success: false, msg: 'Data was not updated successfully'})
                } else {
                    res.json({success: true, msg: 'Data updated successfully', md5: dataFile.md5, id: wf._id})
                }
            });
        } else {
            res.json({sucess: false, msg: 'No Welford task found'})
        }
    });
});

// Get one stat by ID
router.get('/data/:id', function(req, res, next) {
    id = req.params.id;
    
    Welford.getStatsById(id, (err, wf) => {
        if (err) {
            console.log('Error getting the welford statistics with ID: ' + id);
            res.json({success: false, msg: 'Error getting the statistics with id: ' + id, found: false, data: wf});
        } else if (wf != null) {
            const filename = path.join(config.upload_folder, 'welford_' + wf._id + '.npz');
            console.log('Downloading file: ' + filename);

            // Send filename to client
            res.download(filename, 'welford_' + wf._id + '.npz', (err) => {
                if (err){
                    console.log('Download failed from center (Welford)');
                } else {
                    console.log('Download successful (Welford)');
                }
            });
        } else {
            res.json({success: false, msg: 'looks like center ' + id + ' does not exist.'})
        }
    });
});


/* EXPORT TO ROUTES */
module.exports = router;