const express = require('express');

const fileUpload = require('express-fileupload');
const config = require('../config/database')

const router = express.Router();

const Admm = require('../models/admm');


// Compare two sets (different order)
function eqSet(as, bs) {
    if (as.size !== bs.size) return false;
    for (var a of as) if (!bs.has(a)) return false;
    return true;
}

// Get stats
router.get('/', function(req, res, next) {
    Admm.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, admm) {
        if (err){
            res.json({success: false, msg: 'There was an error getting the ADMM component (W_tilde)'})
        } else if (admm != null) {
            res.json({
                success: true, 
                admm: admm, 
                rho: admm.rho,
                id: admm._id, 
                iteration: admm.iteration, 
                done: admm.done,
                busy: admm.busy,
                number_of_centers: admm.number_of_centers,
                md5: admm.md5
            });
        } else {
            return res.json({success: false, admm: null, msg: 'No avaliable ADMM tasks were found.'})
        }
    });

});

// Get W_tilde estimated
router.get('/data/:iteration', function(req, res, next) {
    iteration = req.params.iteration;
    Admm.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, admm) {
        if (err){
            res.json({success: false, msg: 'There was an error getting the statistics'})
        } else if (admm != null && admm.iteration_finished) {
            const filename = config.upload_folder + 'w_tilde_iter_' + iteration + '.npz';
            console.log('Downloading file: ' + filename);

            // Send filename to client
            res.download(filename, 'w_tilde_iter_' + admm.iteration + '.npz', (err) => {
                if (err){
                    console.log('Download failed from center: ' + err.toString());
                } else {
                    console.log('Download successful from center ');
                }
            });
        } else {
            res.json({success: true, msg: 'ADMM is still not avaliable', data: null})
        }
    });

});

// Create a new ADMM study
router.post('/', function(req, res, next) {
    let newAdmm = Admm({
        iteration: 0,
        rho: req.body.rho,
        number_of_iterations: req.body.number_of_iterations,
        centers: Array.from(new Set(req.body.centers)),
        observations: req.body.observations,
        number_of_centers: req.body.centers.length
    });

    Admm.addAdmm(newAdmm, (err, admm) => {
        if (err) {
            console.log(err.toString())
            res.json({success: false, msg: 'ADMM registration failed'});
        } else {
            res.json({success: true, msg: 'ADMM registration successful!', id: admm._id})
        }
    });
});

// Get one stat by ID
router.get('/:id', function(req, res, next) {
    id = req.params.id;
    Admm.getAdmmById(id, (err, admm) => {
        if (err) {
            console.log('Error getting the stats: ' + id);
            res.json({success: false, msg: 'Error getting the ADMM with id: ' + id,});
        } else {
            res.json({success: true, msg: 'Admm found successfully', id: admm._id, iteration: admm.iteration});
        }
    });
});


// Post W_i and alpha_i
router.post('/upload/:id', function(req, res, next) {
    center_id = req.params.id;
    // filename = 'admm_' + center_id + '_iter_' + req.body.iteration +'.npz';
    console.log('Uploading ADMM by:' + center_id)

    // Look for the last ADMM task
    Admm.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, admm) {
        if (err) {
            console.log('Error getting ADMM: ' + err.toString())
            res.json({success: false, msg: 'Error getting ADMM!'})
        } else if (admm != null) {
            if (admm.busy) 
                return res.json({success: false, msg: 'ADMM busy'});

            if(!admm.centers.includes(center_id))
                return res.json({success: false, msg: 'Center not allowed to post!'});

            if (!req.files)
                return res.json({success: false, msg: 'Not file attached. :('});

            // Upload File
            let dataFile = req.files.dataFile;
            console.log(dataFile)
            dataFile.mv(config.upload_folder + dataFile.name, function(err) {
                if (err){
                    console.log('Error moving file:' + err.toString())
                    res.status(500).json({success: false, msg: 'Error getting file!'})
                }
            });

            console.log('File saved on server!')
            // Check if done
            centersSet = new Set(admm.centers);
            doneSet = new Set(admm.centers_done);
            doneSet.add(center_id);
            admm.done = eqSet(doneSet, centersSet) && (admm.iteration >= admm.number_of_iterations);
            // console.log(centersSet)
            // console.log(doneSet)
            // console.log(admm.iteration)
            // console.log(admm.number_of_iterations)
            // console.log('Intersection: ' + doneSet.has(centersSet))
            // console.log('Same iteration: ' + (admm.iteration >= admm.number_of_iterations))
            admm.busy = false;
            admm.centers_done = Array.from(doneSet);
            
            console.log('Center allowed to update\n\tClients done: ' + admm.centers_done + '\n\n')

            admm.save(err => {
                if (err) {
                    console.log('Error updating the database!' + err.toString())
                    res.json({success: false, msg: 'Could not update ADMM in database'})
                } else {
                    res.json({success: true, msg: 'Data updated succesfully', md5: dataFile.md5, id: admm._id})
                }
            });
        } else {
            res.json({success: false, msg: 'No ADMM found!'})
        }
    });
});

// Download individual center data
router.get('/center/:center_id/:iteration', function(req, res, next) {
    center_id = req.params.center_id;
    iteration = req.params.iteration;

    Admm.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, admm) {
        if (err){
            return res.status(500).json({success: false, msg: 'There was an error getting the ADMM component (W_tilde)'})
        } else if (admm != null && !admm.done && admm.centers.includes(center_id) && iteration <= admm.iteration) {
            // Set filename
            filename = 'admm_' + center_id + '_iter_' + iteration + '.npz';
            file_path = config.upload_folder + filename;

            // Send download response
            res.download(file_path, filename)
        } else {
            return res.status(500).json({
                success: false, 
                msg: 'Looks like no active ADMM study has been launched ' + 
                     'or iteration requested is not valid (current iter : ' + admm.iteration + ').'
            });
        }
    });
});


router.put('/master/:status', function(req, res, next) {
    status =req.params.status

    Admm.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, admm) {
        if (err){
            return res.status(500).json({success: false, msg: 'There was an error getting the ADMM component (W_tilde)'})
        } else if (admm != null){
            admm.iteration_finished = Boolean(status);
            admm.save(err => {
                if (err) {
                    console.log('Error updating the database!' + err.toString())
                    res.json({success: false, msg: 'Could not update ADMM in database'})
                } else {
                    res.json({success: true, msg: 'Iteration status changed to: ' + status})
                }
            });
        } else {
            res.json({success: false, msg: 'No ADMM found!'})
        }
    });
});
// Post data (MASTER)
router.post('/master/:id', function(req, res, next) {
    id = req.params.id;
    iteration = req.body.iteration;

    Admm.getAdmmById(id, (err, admm) => {
        if (err) {
            res.json({success: false, msg: 'ADMM process with id ' + id + ' not found'});
        } else if (admm != null) {
            // Check if there is no files to be updated
            if (!req.files){
                return res.json({success: false, msg: 'No files found. No files were uploaded.'});
            }
            // Upload File
            let dataFile = req.files.dataFile;
            // console.log(dataFile)
            // Use the mv() method to place the file somewhere on your server
            dataFile.mv(config.upload_folder + dataFile.name, function(err) {
                if (err){
                    return res.status(500).send(err);
                } else {
                    console.log('File saved on server!\n\n')
                }
            });

            //  Update in database
            admm.centers_done = [];
            admm.iteration = iteration;
            admm.save(err => {
                if (err) {
                    console.log('Error updating the database!' + err.toString())
                    return res.json({success: false, msg: 'Could not update ADMM in database'})
                } else {
                    console.log('Done!')
                    return res.json({success: true, msg: 'File uploaded!', id: admm._id, md5: dataFile.md5});
                }
            });
        } else {
            return res.json({success: false, msg: 'No ADMM id found'});
        }
    });
});

/* EXPORT TO ROUTES */
module.exports = router;