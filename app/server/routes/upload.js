const express = require('express');
const fileUpload = require('express-fileupload');

const config = require('../config/database')
const Upload = require('../models/upload');

const router = express.Router();

// Upload a file
router.post('/', function(req, res, next) {
    let newUpload = new Upload({
        type: req.body.type,
        postedBy: req.body.id,
        description: req.body.description
    });

    // Check if there is files to upload
    if (!req.files)
        return res.json({success: false, msg: 'Not file attached. :('});
    
    // Upload File
    let dataFile = req.files.dataFile;
    console.log(dataFile)
    dataFile.mv(config.upload_folder + dataFile.name, function(err) {
        if (err){
            console.log('Error moving file:' + err.toString())
            return res.status(500).json({success: false, msg: 'Error getting file!'})
        }
    });

    // Register upload
    newUpload.md5 = dataFile.md5

    Upload.addUpload(newUpload, (err, upload) => {
        if (err) {
            console.log(err.toString())
            res.json({success: false, msg: 'Upload failed!'});
        } else {
            res.json({success: true, msg: 'Upload successful!', id: upload._id, md5: upload.md5})
        }
    });
});


// Download a file
router.get('/:type/:id/', function(req, res, next) {
    filename = config.upload_folder + req.params.type + '_' + req.params.id + '.npz'
    console.log('Trying to download: ' + filename)

    // Send file from server to client
    res.download(filename, (err) => {
        if (err){
            console.log('Download failed from center');
        } else {
            console.log('Download successful');
        }
    });
});

/* EXPORT TO ROUTES */
module.exports = router;