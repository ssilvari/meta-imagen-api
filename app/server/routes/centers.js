const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');

const config = require('../config/database')

const Center = require('../models/center');


/* GET center listing. */
router.get('/', function(req, res, next) {

    Center.find({}, function(err, centers) {
        var centerMap = {};

        centers.forEach(function(center) {
            centerMap[center._id] = {
                center: center._id,
                enabled: center.enabled,
                numberOfSubjects: center.numberOfSubjects
            };
        });

        res.json(centerMap);
    });
});


router.get('/:id', function(req, res, next) {
    id = req.params.id;
    Center.getCenterById(id, (err, center) => {
        if (err) {
            console.log('Error getting the center: ' + id);
            res.json({success: false, msg: 'Looks like center ' + id + ' does not exist.'})
        } else if (center != null){
            res.json({success: true, data: center});
        } else {
            res.json({success: false, msg: 'Looks like center ' + id + ' does not exist.'})
        }
    });
});

// Create a center
router.post('/new', function(req, res, next) {
    let newCenter = new Center({
        email: req.body.email,
        password: req.body.password,
        info: req.body.info,
        enabled: req.body.enabled
    });

    Center.addCenter(newCenter, (err, center) => {
        if (err) {
            res.json({success: false, msg: 'Registration Failed'})
        } else {
            res.json({success: true, msg: 'Center registered'})
        }
    })
});

// POST STATISTICS
router.post('/statistics/:id', function(req, res, next) {
    id = req.params.id;

    console.log('Posting PLSR statistics | Center: ' + id);
    Center.getCenterById(id, (err, center) => {
        if (err) {
            console.log('Error getting the center: ' + id);
            res.json({success: false, msg: 'Error getting the center: ' + id})
        } else if (center.enabled){
            if (!req.files){
                return res.json({success: false, msg: 'No files were uploaded.'});
            }

            // Update version
            center.version ++;

            let dataFile = req.files.dataFile;
            let filename = 'stats_' + center._id + '_v' + center.version;

            console.log(dataFile);
            dataFile.mv(config.upload_folder + filename + '.npy', function(err) {
                if (err)
                    return res.status(500).send(err);
            });

            center.statistics.push(dataFile.md5);

            center.save(err => {
                if (err) {
                    res.json({success: false, msg: 'Data was not updated successfully'})
                } else {
                    res.json({success: true, msg: 'Data updated successfully', md5: dataFile.md5})
                }
            });

        } else {
            res.json({success: false, msg: 'Looks like this center is not allowed to share data'})
        }
    });
});


// GET STATISTICS
router.get('/statistics/:id', function(req, res, next) {
    id = req.params.id;

    console.log('Getting PLSR statistics | Center: ' + id);
    Center.getCenterById(id, (err, center) => {
        if (err) {
            console.log('Error getting the center: ' + id);
            res.json({success: false, msg: 'Error getting the center: ' + id})
        } else if (center != null && center.enabled){
            const filename = path.join(config.upload_folder, 'stats_' + center._id + '_v' + center.version + '.npy');
            console.log('Downloading file: ' + filename);

            // Send filename to client
            res.download(filename, 'statistics.npy', (err) => {
                if (err){
                    console.log('Download failed from center: ' + id);
                } else {
                    console.log('Download successful from center ' + id);
                }
            });
        } else {
            res.json({success: false, msg: 'Looks like this center is not allowed to share data'})
        }
    });
});

// Export module
module.exports = router;
