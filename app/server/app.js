const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const passport = require('passport');
const fileUpload = require('express-fileupload');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const centersRouter = require('./routes/centers');
const welfordRouter = require('./routes/welford');
const admmRouter = require('./routes/admm');
const uploadsRouter = require('./routes/upload');

const config = require('./config/database');

const port = 3300;

// Connect to a Mongo Database
mongoose.connect(config.database);
mongoose.connection.on('connected', () => {
    console.log('[  OK  ] Connected to database');
});
mongoose.connection.on('error',(err) => {
    console.log('[  ERROR  ] Database error: ' + err);
});


const app = express();
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(logger('dev'));
app.use(express.json({limit: '1000mb'}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Body Parser Middleware
app.use(bodyParser.json());

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

// File Uploader middleware
app.use(fileUpload());

// Routes
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/centers', centersRouter);
app.use('/welford', welfordRouter);
app.use('/admm', admmRouter);
app.use('/upload', uploadsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// ===== TODO: HTTPS ====
// const fs = require('fs');
// const https = require('https');
// const privateKey  = fs.readFileSync('/keys/privkey.pem', 'utf8');
// const certificate = fs.readFileSync('/certs/fullchain.pem', 'utf8');

// const credentials = {key: privateKey, cert: certificate};
// var httpsServer = https.createServer(credentials, app);
// httpsServer.listen(port);
//  ==== END HTTPS ====

// Listen port 3300
app.listen(port, function () {
    console.log('Example app listening on port ' + port + '!');
});

module.exports = app;
