const express = require('express');
const fileUpload = require('express-fileupload');

const router = express.Router();

const Admm = require('../models/admm');


// Get stats
router.get('/', function(req, res, next) {
    Admm.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, post) {
        if (err){
            res.json({success: false, msg: 'There was an error getting the ADMM component (W_tilde)'})
        } else {
            res.json({success: true, msg: post})
        }
    });

});

// Get one stat by ID
router.get('/:id', function(req, res, next) {
    id = req.params.id;
    Admm.getStatsById(id, (err, stats) => {
        if (err) {
            console.log('Error getting the stats: ' + id);
            res.json({success: false, msg: 'Error getting the ADMM with id: ' + id, found: false, data: stats});
        } else {
            res.json({success: true, msg: 'Admm found successfully', found: true, data: stats});
        }
    });
});


// Post stats
router.post('/upload', function(req, res, next) {
    filename = 'w_tilde_' + Date.now();

    let newAdmm = Admm({
        w_tilde: filename
    });

    if (!req.files){
        return res.json({success: false, msg: 'No files were uploaded.'});
    }
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let sampleFile = req.files.sampleFile;
    console.log(sampleFile)

    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv('/user/ssilvari/home/node/uploads/' + filename + '.npy', function(err) {
        if (err)
            return res.status(500).send(err);

        res.json({success: true, msg: 'File uploaded!'});
    });
});


/* EXPORT TO ROUTES */
module.exports = router;