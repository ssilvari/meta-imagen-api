const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

const config = require('../config/database')

const Center = require('../models/center');


/* GET center listing. */
router.get('/', function(req, res, next) {

    Center.find({}, function(err, centers) {
        var centerMap = {};

        centers.forEach(function(center) {
            centerMap[center._id] = {
                center: center._id,
                enabled: center.enabled,
                numberOfSubjects: center.numberOfSubjects
            };
        });

        res.json(centerMap);
    });
});


router.get('/:id', function(req, res, next) {
    id = req.params.id;
    Center.getCenterById(id, (err, center) => {
        if (err) {
            console.log('Error getting the center: ' + id);
            res.json({})
        } else {
            res.json(center);
        }
    });
});

router.put('/:id', function(req, res, next) {
    id = req.params.id;
    Center.getCenterById(id, (err, center) => {
        if (err) {
            console.log('Error getting the center: ' + id);
        } else {
            center.avX = req.body.avX;
            center.stX = req.body.stX;
            center.avY = req.body.avY;
            center.stY = req.body.stY;
            center.comp = req.body.comp;
            center.weights = req.body.weights;
            center.numberOfSubjects = req.body.numberOfSubjects;
            center.updated = Date.now();
            center.save(err => {
                if (err) {
                    res.json({success: false, msg: 'Data was not updated successfully'})
                } else {
                    res.json({success: true, msg: 'Data updated successfully'})
                }
            })
        }
    });

});

// Create a center
router.post('/', function(req, res, next) {
    let newCenter = new Center({
        center_id: req.body.center_id,
        info: req.body.info,
        avX: req.body.avX,
        stX: req.body.stX,
        avY: req.body.avY,
        stY: req.body.stY,
        comp: req.body.comp,
        weights: req.body.weights,
        numberOfSubjects: req.body.numberOfSubjects,
        enabled: req.body.enabled,
        password: req.body.password
    });

    Center.addCenter(newCenter, (err, center) => {
        if (err) {
            res.json({success: false, msg: 'Registration Failed'})
        } else {
            res.json({success: true, msg: 'Center registered'})
        }
    })
});


// Post stats
router.post('/admm', function(req, res, next) {
    filename = 'center_' + Date.now();

    if (!req.files){
        return res.json({success: false, msg: 'No files were uploaded.'});
    }
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let sampleFile = req.files.sampleFile;
    console.log(sampleFile);

    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv(config.upload_folder + filename + '.npy', function(err) {
        if (err)
            return res.status(500).send(err);

        res.json({success: true, msg: 'File uploaded!', file: filename});
    });
});


//Authenticate
router.post('/auth', function(req, res, next) {
    const center_id = req.body.id;
    const key = req.body.key;

    Center.getCenterById(center_id, (err, center) => {
        if (err) throw err;
        if (!center) {
            return res.json({success: false, msg: 'Center was not found. Contact support'})
        }

        Center.compareKey(key, center.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                const token = jwt.sign(center.toJSON(), config.secret, {
                    expiresIn: 604800 // 1 Week
                });
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    center: {
                        id: center._id
                    }
                });
            } else {
                res.json({success: false, msg: 'It was not able to log in'});
            }
        });
    });
});

// Get data from one server

module.exports = router;
