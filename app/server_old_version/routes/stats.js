const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

const Stats = require('../models/stats');


// Get stats
router.get('/', function(req, res, next) {
    Stats.findOne({}, {}, { sort: { 'updated' : -1 } }, function(err, post) {
        if (err){
            res.json({success: false, msg: 'There was an error getting the statistics'})
        } else {
            res.json({success: true, msg: post})
        }
    });

});

// Get one stat by ID
router.get('/:id', function(req, res, next) {
    id = req.params.id;
    Stats.getStatsById(id, (err, stats) => {
        if (err) {
            console.log('Error getting the stats: ' + id);
            res.json({success: false, msg: 'Error getting the statistics with id: ' + id, found: false, data: stats});
        } else {
            res.json({success: true, msg: 'Stats found successfully', found: true, data: stats});
        }
    });
});


// Post stats
router.post('/', function(req, res, next) {
    let newStats = new Stats({
        currentAvgX: req.body.currentAvgX,
        currentStdX: req.body.currentStdX,
        currentAvgY: req.body.currentAvgY,
        currentStdY: req.body.currentStdY,
        currentK: req.body.currentK,
        updatedBy: req.body.updatedBy,
        busy: req.body.busy
    });

    Stats.addStats(newStats, (err, stats) => {
        if (err) {
            res.json({success: false, msg: 'Stats creation failed', id: null})
        } else {
            res.json({success: true, msg: 'Stats registered', id: newStats['_id']})
        }
    })
});


/* EXPORT TO ROUTES */
module.exports = router;