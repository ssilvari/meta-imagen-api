const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('../config/database')


const StatsSchema = mongoose.Schema({
    currentAvgX: {
        type: [Number],
        required: true
    },
    currentStdX: {
        type: [Number],
        required: true
    },
    currentAvgY: {
        type: [Number],
        required: true
    },
    currentStdY: {
        type: [Number],
        required: true
    },
    currentK: {
        type: Number,
        required: true
    },
    updatedBy: {
        type: String,
        required: true
    },
    busy: {
        type: Boolean,
        required: true
    },
    updated: {
        type: Date, default: Date.now
    }
});

const Stats = module.exports = mongoose.model('Stats', StatsSchema);

module.exports.addStats = function (newStats, callback) {
    newStats.save(callback);
};

module.exports.getStatsById = function (id, callback) {
    Stats.findById(id, callback);
};
