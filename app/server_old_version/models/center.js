const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const crypto = require('crypto');

const config = require('../config/database');
var token = crypto.randomBytes(64).toString('hex');

const CenterSchema = mongoose.Schema({
    center_id: {
        type: Number,
        required: true,
        unique: true
    },
    info: {
        type: String,
        required: false
    },
    avX: {
        type: [Number],
        required: true
    },
    stX: {
        type: [Number],
        required: true
    },
    avY: {
        type: [Number],
        required: true
    },
    stY: {
        type: [Number],
        required: true
    },
    comp: {
        type: [[[Number]]],
        required: true
    },
    weights: {
        type: [[[Number]]],
        required: true
    },
    admm_w: {
        type: String,
        required: false
    },
    admm_alpha: {
        type: String,
        required: false
    },
    numberOfSubjects: {
        type: Number,
        required: true
    },
    enabled: {
        type: Boolean,
        required: true
    },
    updated:{
        type: Date, default: Date.now
    },
    password: {
        type: String,
        required: true
    }
});

const Center = module.exports = mongoose.model('Center', CenterSchema);

module.exports.getCenterById = function (id, callback) {
    Center.findById(id, callback);
};

module.exports.addCenter = function (newCenter, callback) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newCenter.password, salt, (err, hash) => {
            if (err) throw err;
            newCenter.password = hash;
            newCenter.save(callback);
        })
    });
};

module.exports.compareKey = function (candidateKey, hash, callback) {
    bcrypt.compare(candidateKey, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
};
