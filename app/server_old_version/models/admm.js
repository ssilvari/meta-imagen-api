const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const AdmmSchema = mongoose.Schema({
    w_tilde: {
        type: String,
        required: true
    },
    iteration: {
        type: Number,
        required: true
    },
    busy: {
        type: Boolean,
        default: false,
        required: false
    },
    observations: {
        type: String,
        required: false
    },
    updated:{
        type: Date, default: Date.now
    }
});

const Admm = module.exports = mongoose.model('Admm', AdmmSchema);

module.exports.getAdmmById = function (id, callback) {
    Admm.findById(id, callback);
};

module.exports.addAdmm = function (newAdmm, callback) {
    newAdmm.save(callback);
};
