const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const config = require('../config/database');

const Center = require('../models/center');

module.exports = function (passport) {
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = config.secret;

    passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
        Center.getCenterById(jwt_payload._doc._id, function(err, center) {
            if (err) {
                return done(err, false);
            }
            if (center) {
                return done(null, center);
            } else {
                return done(null, false);
                // or you could create a new account
            }
        });
    }));
};
