import os
import time
import logging

import shutil
import hashlib
import requests

import numpy as np

from requests.exceptions import ConnectionError

# ======= FUNCTION DEFINITIONS =======
def md5_checksum(file_path):
    with open(file_path, 'rb') as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()

def get_data_folder():
    try:
        return os.environ['DATA_FOLDER']
    except KeyError:
        return './output'

def get_server_url():
    try:
        return os.environ['API_HOST'] + 'admm'
    except KeyError:
        return 'http://localhost:3300/admm/'

def get_admm_params():
    '''
        Gets the id of the centers involved and
        the current iteration.
    '''
    r = requests.get(get_server_url())

    if r.status_code is 200:
        res = r.json()
        if res['success']:
            busy = res['busy']
            centers = set(res['admm']['centers'])
            centers_done = set(res['admm']['centers_done'])

            print('centers: {} | centers done: {}'.format(centers, centers_done))

            if len(centers_done) < len(centers) or busy:
                logger.warning('Looks like the centers have not finished posting data.')
                return None
            elif centers_done == centers:
                logger.info('Starting W_tilde calculation...')
                return res['admm']['centers'], res['iteration'], res['rho'], res['id'], res['admm']['number_of_iterations'], res['number_of_centers']
            else:
                logger.error('The centers involved in this study are not the same that published')
                return None
        else:
            return None
    else:
        msg = 'Connection error. Status code: {}'.format(r.status_code)
        print('[  ERROR  ] ' + msg)
        logger.error(msg)
        return None

def gather_centers_data(centers, iteration):
    '''
        Download each center data from API
        and stack it for calculation
    '''
    main_url = get_server_url() + '/center'
    alpha_k = {}
    W_k = {}

    success = False # Flag for not retrying
    while not success:
        for center in centers:
            url = main_url + '/%s/%s' % (center, iteration)
            dl_data_file = os.path.join(get_data_folder(), 'admm_%s_iter_%d.npz' % (center, iteration))
            try:
                r = requests.get(url, stream=True)
                logger.info('Downloading data for center %s...' % center)
                
                if r.status_code is 200:
                    with open(dl_data_file, 'wb') as f:
                        r.raw.decode_content = True
                        shutil.copyfileobj(r.raw, f)
                    
                    # Check data origin
                    data = np.load(dl_data_file)
                    if (data['id'] == center):
                        logger.info(
                            'Data correctly downloaded: \n\t- Filename: %s\n\t- ID: %s\n\t- Mean: %f\n\t- Std: %f'
                            % (dl_data_file, data['id'], np.mean(data['alpha_i']), np.std(data['alpha_i'])))
                        
                        # Stack data
                        alpha_k[center] = data['alpha_i']
                        W_k[center] = data['W_i']
                    else:
                        logger.error('Data does not come from the expected center.')

                elif r.status_code is 500:
                    try:
                        res = r.json()
                        logger.error('Connection error 500: {}'.format(res['msg']))
                    except KeyError:
                        logger.error('Connection error 500 without response')
                else:
                    logger.error('Connection error. Status code: %s' % r.status_code)
            except ConnectionError as e:
                logger.error('Connection error. {}'.format(e))
        
        # Check output
        logger.debug('Length: centers %d | alpha: %d | W: %d ' % (len(centers), len(alpha_k), len(W_k)))
        os.system('tail ' + os.path.join(get_data_folder(), 'admm_master.log')) # TODO: Remove
        success = (len(centers) == len(alpha_k) and len(centers) == len(W_k))
        if not success:
            time.sleep(5)
    
    logger.info('Centers: {} | Number: {}'.format(centers, len(centers)))
    logger.info('Alpha: {} | Number: {}'.format(alpha_k.keys(), len(alpha_k)))
    logger.info('W: {} | Number: {}'.format(W_k.keys(), len(W_k)))
    
    # assert len(centers) == len(alpha_k)
    alpha_k = [val for key, val in alpha_k.items()]
    W_k = [val for key, val in W_k.items()]
    return alpha_k, W_k

def admm_update_w_tilde(alpha_k, W_k, rho, m):
    '''
        Calculates the estimated W (W_tilde)
        from centers data iteration
    '''
    # Check size
    alpha_k = np.array(alpha_k)
    W_k = np.array(W_k)
    assert alpha_k.shape == W_k.shape

    # Calculate W_tilde
    W_tilde = np.sum(alpha_k / rho + W_k, axis=0) / m
    logger.debug(
        'W_tilde updated:\n\t- Mean: %f\n\t- Std: %f'
        % (W_tilde.mean(), W_tilde.std())
    )

    # Return it
    return W_tilde

def upload_to_api(W_tilde, iteration, id, n_iter):
    '''
        Uploads W_tilde calculated to server and
        updates params (iteration, etc.)
    '''
    
    # Save W_tilde as npz file
    w_tilde_file = os.path.join(get_data_folder(), 'w_tilde_iter_%d.npz' % iteration)
    logger.info('Saving W_tilde as: ' + w_tilde_file)
    np.savez_compressed(w_tilde_file, W_tilde=W_tilde, iter=iteration)

    # Upload it!
    # try 10 times
    url = get_server_url() + '/master/{}'.format(id)
    
    # Update iteration
    iteration += 1

    success = False
    while not success:
        # Create metadata
        metadata = {
        'iteration': iteration,
        }
        files = {'dataFile': open(w_tilde_file, 'rb')}
    
        # Send the POST request
        r = requests.post(url, files=files, data=metadata)

        # Check the operation
        if r.status_code is 200:
            res = r.json()
            msg = res['msg']
            print(res)

            if res['success']:
                logger.info(msg)
                # Check if everything is ok
                _, online_iteration, _, _, _, _ = get_admm_params()
                if online_iteration == iteration:
                    logger.info('Update saved in ADMM with id: {}'.format(res['id']))
                    logger.info('Transmission finished')
                    success = True
            else:
                logger.error('Transmission to server was not successful:\n\t- Message: %s' % msg)
        elif r.status_code is not 200:
            logger.error('There is an HTTP error - Status bin: {}'.format(r.status_code))
        
        logger.info('Something uploading W_tilde went wrong. Trying to do it again...')
        time.sleep(2)

def check_if_done():
    url = get_server_url()
    try:
        r = requests.get(url)

        if r.status_code is 200:
            res = r.json()
            if res['success']:
                return res['done']
            else:
                return False
        else:
            logger.error(
                'Looks like there is a problem in the API server. \n' +\
                'If persists, contact support.\n' + \
                'Error code: {}'.format(r.status_code))
    except ConnectionError as e:
        logger.error('Connection error. {}'.format(e))

def set_iteration_done(status):
    url = get_server_url() + '/master/' + str(status).lower()
    print('Accessing to (iteration_finished):', url)

    r = requests.put(url)

    while r.status_code is not 200:
        logger.info('Setting iteration status failed. CODE: {}'.format(r.status_code))
        r = requests.put(url)

def main():
    '''
        Here the magic happens!
        Main function that performs ADMM W_tilde calculation
        after collecting all centers data
    '''
    try:
        if not check_if_done():
            # Collect centers involved
            centers, iteration, rho, id, n_iter, m = get_admm_params()

            # Block centers from posting data
            set_iteration_done(False)

            # Download data
            alpha_k, W_k = gather_centers_data(centers, iteration)

            # Update W_tilde
            W_tilde = admm_update_w_tilde(alpha_k, W_k, rho, m)

            # Upload results
            upload_to_api(W_tilde, iteration, id, n_iter)
            
            # Enable centers to download and post
            set_iteration_done(True)
        else:
            t_sleep = 30
            logger.info('ADMM Finished (waiting %.1f s to check)')
            print('ADMM Finished (waiting %.1f s to check)')
            time.sleep(t_sleep)
    except TypeError:
        logger.info('Waiting for retry...\n\n')
        return False
    except ConnectionError as e:
        logger.error('Connection error: {}'.format(e))
    

if __name__ == '__main__':

    # ======= SetUp and start logger =======
    logger = logging.getLogger(__name__)
    level = logging.DEBUG
    logger.setLevel(level)

    log_file = os.path.join(get_data_folder(), 'admm_master.log')
    handler = logging.FileHandler(log_file)
    handler.setLevel(level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    print('Log file located at: ' + log_file)

    # ======= START ADMM (MASTER) =======
    done = False
    while not done:
        done = main()
        if not done:
            os.system('tail ' + log_file)
            logger.info('Waiting for API to update...')
            time.sleep(5)
    
    print('[  OK  ] DONE!')

    # Show console
    msg = 'DONE!\n\n\n'
    print(msg)
    logger.info(msg)
    os.system('cat ' + log_file)